# coding=utf-8
import urllib2
import urllib
import json
import subprocess
import re
from urlparse import urlparse
import os.path

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	# print urlparse(string)
	# print resultString
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")

# netflixSeriesGraber
# @seriesid: for debug use
# @isCollection: default false; only true when used for loop collections
# @title: title of series
# @year: release year
# @country: release country
# @season: season to search
# @return: {"log":"...", "episodes": {"1":{"url":"https://page_url", "epTitle":"xxx"}, ..., "epNum":"12"}(, "collectionSeasonList":[2, 3] or [-1]), 'netflix_series':'seriesid'} 
#          url:None if no result
def netflixSeriesGraber(seriesid, isCollection, title, country, year, season):
	title = encodeHelper(title)
	proc = subprocess.Popen('node netflix_series.js'+" "+isCollection+" "+title+" "+country+" "+year+" "+season, stdout=subprocess.PIPE, shell=True)
	# print re.sub(r"\n", "", proc.stdout.read())
	response = eval(re.sub(r"\n", "", proc.stdout.read()))
	response["netflix_series"] = seriesid
	return response

if __name__ == "__main__":
	seriesid = "xxx"
	isCollection = "true"
	seriesTitle = "good eats"
	country = "USA"
	year = "2012"
	season = "-1"
	result = netflixSeriesGraber(seriesid, isCollection, seriesTitle, country, year, season)
	print result