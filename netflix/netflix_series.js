// Di Yang
// diy@usc.edu
/*
  Usage: Usage (command-line): node netflix_series.js isCollection Title Country Year Season
     isCollection could only be true when known it's collection and need another loop through all series
*/
var url_netflix_search = "";
var resultObj = null;
var title = "";
var country = null;
var year = null;
var season = null;
var found = false;
var isCollection = ""
var collectionSeasonList = null

var Nightmare = require('nightmare');

//check if a search parameter is correct
if (process.argv.length < 7) {
	console.log('Usage: node netflix_series.js isCollection Title Country Year Season');
	return;
}else {
    var length = process.argv.length;
    var isCollection = process.argv[2]
    for (var i=3; i<length-4; i++){
        title += process.argv[i] + " ";
    }
	
    title += process.argv[length-4];
    country = process.argv[length-3];
    if (country === "USA") { // US and UK country
        country = "U.S.";
    } else if (country === "UK") {
        country = "U.K.";
    }
    year = process.argv[length-2];
    season = process.argv[length-1];
    url_netflix_search = "http://www.netflix.com/api/desktop/search/instantsearch?esn=www&term="+title;
    title = decodeURIComponent(title);
    console.log('{"log":');
    console.log("\"STEP-1 Login Netflix and search for title...");
}
new Nightmare({cookiesFile: "./cookie.txt"})
    .on('timeout', function(msg) {
        return console.log(msg);
    })
    .viewport(1366, 768)
    .on('error', function(msg, trace) {
        return "NETWORK_ERROR";
    })
    .useragent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
    // STEP-1 Login & Search
    .goto(url_netflix_search)
    .screenshot('./1_afterSearch.png')
    .evaluate(function (title, country) {
            if (!JSON.parse(document.getElementsByTagName("pre")[0].textContent).hasOwnProperty('galleryVideos')) {
                return -1;
            }
            var showList = JSON.parse(document.getElementsByTagName("pre")[0].textContent).galleryVideos.items;
            if (showList.length == 1) {
                if (showList[0].title.toUpperCase() === title.toUpperCase()) { // 1 result; perfectly match
                    window.location.href = "http://www.netflix.com/WiMovie/"+showList[0].id;
                    return 1;
                } else if (showList[0].title.toUpperCase().indexOf(title.toUpperCase()) != -1) { // 1 result; contain title
                    window.location.href = "http://www.netflix.com/WiMovie/"+showList[0].id;
                    return 2;
                } else if (showList[0].title.toUpperCase().indexOf(title.toUpperCase()) == -1) { // 1 result;  not contain title
                    window.location.href = "http://www.netflix.com/WiMovie/"+showList[0].id;
                    return 3
                }
            } else { // multiple results
                // star trek
                for (var i = 0; i < showList.length; i++) {
                    if (showList[i].title.toUpperCase() === title.toUpperCase()) {
                        window.location.href = "http://www.netflix.com/WiMovie/"+showList[i].id;
                        return 4; // exist perfect match!
                    }
                }
                title = title+" ("+country+")";
                for (var i = 0; i < showList.length; i++) {
                    if (showList[i].title.toUpperCase() === title.toUpperCase()) {
                        window.location.href = "http://www.netflix.com/WiMovie/"+showList[i].id;
                        return 4; // exist perfect match with country!
                    }
                }

            }
        }, function (result) {
            if (result == -1) {
                console.log("STEP-1 0 result! Title not found!");
            } else if (result == 1) {
                console.log("STEP-1 1 result! Titles PERFECT match!");
            } else if (result == 2) {
                console.log("STEP-1 1 result! Titles PARTIAL match!");
            } else if (result == 3) {
                console.log("STEP-1 1 result! Titles DON'T match!");
            } else if (result == 4) {
                console.log("STEP-1 Multiple results! Exsits PERFECT match!");
            } else {
                console.log("STEP-1 Multiple results! NO PERFECT match so cannot determine!");
            }
        }, title, country
    )
    .wait(1000)
    .screenshot('./2_homepage.png')
    // STEP-2 Choose Season (no year check this step)
    .evaluate(function (season, isCollection) {
            // click season item (if exists)
            var backgroundClass = document.getElementsByTagName("html")[0].getAttribute("class");
            if (backgroundClass == null) {
                return -1;
            }
            if (backgroundClass === " en") {
                // white background
                if (isCollection == "false") {
                    if (document.querySelector("h1[class='title']").textContent.indexOf("Collection") != -1) { // include "Collection", need not choose season, but need return all collection seasons
                        if (document.getElementsByClassName("selectorItems").length) { // have season button
                            seasonList = document.getElementsByClassName("seasonItem");
                            resultList = []
                            for (var i = 0; i < seasonList.length; i++) {
                                resultList.push(seasonList[i].firstChild.textContent)
                            }
                            return resultList.length == 0 ? ['-1'] : resultList;
                        } else {
                            return ['-1'];
                        }
                    }
                }
                if (isCollection == "true" && season == -1) { // collection only one season
                    return true
                }
                if (document.getElementsByClassName("selectorItems").length) { // have season button
                    if (document.getElementsByClassName("seasonItem").length) { // have season drop down items
                        var seasonList = document.getElementsByClassName("seasonItem");
                        for (var i = 0; i < seasonList.length; i++) { // loop season items
                            if (seasonList[i].firstChild.textContent === season) { // find correct season
                                seasonList[i].click();
                                return true;
                            }
                        }
                        return -2; // no correct season 
                    } else if (document.getElementsByClassName("selectorTxt")[0].textContent !== season) { // no season drop down items, and season incorrect
                        return -2;
                    } else { // no season drop down itmes, and season correct
                        return true;
                    }
                } else { // doesn't have season button, no need to click
                    return true;
                } 
            } else if (backgroundClass.indexOf("black") != -1) {
                // black background -- no need to click season!
                if (document.getElementsByClassName("qdropdown-trigger").length) { // have season button (=> have season options)
                    var seasonList = document.getElementsByClassName("option");
                    for (var i = 0; i < seasonList.length; i++) { // loop season options
                        if (seasonList[i].firstChild.firstChild.textContent === season) { // find correct season
                            seasonList[i].firstChild.click();
                            return true;
                        }
                    }
                    return -2; // no correct season
                } else { // doesn't have season button, no need to click (assuming correct because of netflix orignal)
                    return true;
                }
            }
        }, function (result) {
            // console.log(result);
            if (Array.isArray(result)) {
                collectionSeasonList = result
                found = true
                // console.log(collectionSeasonList)
                console.log('STEP-2 Choose Season -- Getting collection seasons...');
            } else if (result == -1) {
                console.log('STEP-2 Choose Season -- No class on <html>!');
            } else if (result == -2) {
                console.log('STEP-2 Choose Season -- Season not found!');
            } else if (result == true) {
                console.log('STEP-2 Choose Season -- Clicking Season '+season+'...');
            } else {
                console.log('STEP-2 Choose Season -- new background page!!!!!!!!');
            }
        }, season, isCollection
    )
    .wait(2000)
    .screenshot('./3_afterClickSeason.png')

    // STEP-3 Get Episodes (and year check for the season year)
    .evaluate(function (season, year, isCollection) {
            if (document.getElementsByTagName("html")[0].getAttribute("class") == null) {
                return -1;
            }
            var backgroundClass = document.getElementsByTagName("html")[0].getAttribute("class");
            if (backgroundClass === " en") {
                // white background
                if (isCollection == "false" && document.getElementsByClassName("year")[1].textContent !== year) {
                    return -2;
                }
                var episodeList = document.getElementsByClassName("episodeTitle");
                var result = "{"; // {epNum:16, "episode title":{"Episode":"1", "url":"xxx"}, 2:{...} ...}
                result += '"epNum":"'+episodeList.length+'"';
                for (var i=0; i<episodeList.length; i++) {
                    var epNumber = episodeList[i].parentNode.firstChild.textContent;
                    var url = "http://www.netflix.com/watch/"+episodeList[i].parentNode.getAttribute("data-episodeid");
                    result += ',"'+epNumber+'":{"url":"'+url+'","epTitle":"'+episodeList[i].textContent+'"}';
                }
                result += "}";
                return result;
            } else if (backgroundClass.indexOf("black") != -1) {
                // black background !!!!!!!!!!!!!!!!!test!!!!!!!!!!!!!
                // Assume seasons are continuous, so use magic number here!
                return -1;
            } else {
                return -3;
            }
        }, function (result) {
            if (result == -1) {
                console.log('STEP-3 No class on <html>!",');
            } else if (result == -2) {
                console.log('STEP-3 Get Season year not match!",');
            } else if (result == -3) {
               console.log('STEP-3 Get New page type!",'); 
            } else {
                found = true
                resultObj = result;
                console.log('STEP-3 Year match! Getting episodes ...",');
            }
        }, season, year, isCollection
    )
    .run(function (err, nightmare) {
    	if (err) return console.log(err);
        // STEP-4 Result
    	if (found) {
            if (collectionSeasonList != null) {
                console.log('"collectionSeasonList":')
                console.log(collectionSeasonList)
                console.log(",")
            }
            console.log('"episodes":');
            if (resultObj == null) {
                console.log('"None"')
            } else {
                console.log(resultObj);
            }
            console.log('}');
    	}else { // no result key in json
            console.log('"episodes":"None"}')
    	}
    });