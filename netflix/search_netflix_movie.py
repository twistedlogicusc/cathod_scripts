# coding=utf-8
import urllib2
import urllib
import json
import subprocess
from urlparse import urlparse
import re

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	# print urlparse(string)
	# print resultString
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")


# netflixMovieGraber
# @title: title of movie
# @return: {'url': 'https://page_link' (or "None"), "log":"STEP-1 -- ....", "netflix_movie":'movieid'}
def netflixMovieGraber(movieid, title):
	title = encodeHelper(title)
	proc = subprocess.Popen('node netflix_movie.js'+" "+title, stdout=subprocess.PIPE, shell=True)
	response = eval(re.sub(r"\n", " ", proc.stdout.read()))
	response["netflix_movie"] = movieid
	return response

if __name__ == "__main__":
	movieid = "xxx"
	title = "star wars: the clone wars"
	print netflixMovieGraber(movieid, title)