# Filename: GlobalHelperFunction.py
# Author: Di Yang
# Email: twistedlogicuestc@gmail.com

from urlparse import urlparse
import urllib
import datetime

# helper function, encoding for amazon signing
def encodeHelper(string):
    resultString = ""
    if urlparse(string).scheme != "":
        resultString += urlparse(string).scheme+":"
    if urlparse(string).netloc != "":
        resultString += "//"+urlparse(string).netloc
    if urlparse(string).path != "":
        resultString += urlparse(string).path
    if urlparse(string).params != "":
        resultString += ";"+urlparse(string).params
    if urlparse(string).query != "":
        resultString += "?"+urlparse(string).query
    if urlparse(string).fragment != "":
        resultString += "#"+urlparse(string).fragment
    return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")

# helper function, get timestamp for amazon
def getTimestamp():
    timestamp_raw = datetime.datetime.utcnow().isoformat().split(".")
    timestamp = urllib.quote(bytes(timestamp_raw[0]+"Z").encode('utf-8'))
    return timestamp

# helper fucntion, month to number
def monthToNum(date):
    return{
        'Jan' : "01",
        'Feb' : "02",
        'Mar' : "03",
        'Apr' : "04",
        'May' : "05",
        'Jun' : "06",
        'Jul' : "07",
        'Aug' : "08",
        'Sep' : "09", 
        'Oct' : "10",
        'Nov' : "11",
        'Dec' : "12"
    }[date]

# infoGraber helper function for iTunes
# @item: json item to grab
# @return: {'url': 'https://page_link',
#           'BuySD': 9.99, (optional)
#           'BuyHD': 9.99, (optional)
#           'RentSD': 2.99, (optional)
#           'RentHD': 3.99, (optional)}
#           'log':'xxx'
#          None if no result
def iTunesInfoGraber(item, result):
    result["url"] = item["trackViewUrl"]
    if "trackHdPrice" in item and item["trackHdPrice"] != -1:
        result["BuyHD"] = item["trackHdPrice"]
    if "trackHdRentalPrice" in item and item["trackHdRentalPrice"] != -1:
        result["RentHD"] = item["trackHdRentalPrice"]
    if "trackPrice" in item and item["trackPrice"] != -1:
        result["BuySD"] = item["trackPrice"]
    if "trackRentalPrice" in item and item["trackRentalPrice"] != -1:
        result["RentSD"] = item["trackRentalPrice"]
    return result

# Amazon configuration class
class AmazonConfig(object):
    # Class variables
    browseNode = "2858778011" # instant video, need to check now and then!!!!!!!
    responseGroup_itemSearch = encodeHelper("ItemAttributes")
    service = "AWSECommerceService"
    searchIndex = "VHS"
    associateTag = "cathod07-20"
    awsAccessKeyId = urllib.quote(bytes("AKIAIPOUFFWOLIKM7MFA").encode('utf-8'))
    awsSecretKey = urllib.quote(bytes("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").encode('utf-8'))
    operation_itemSearch = "ItemSearch"
