# coding=utf-8
import urllib2
import urllib
import json
from urlparse import urlparse

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	# print urlparse(string)
	# print resultString
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")

# helper fucntion, month to number
def monthToNum(date):
	return{
        'Jan' : "01",
        'Feb' : "02",
        'Mar' : "03",
        'Apr' : "04",
        'May' : "05",
        'Jun' : "06",
        'Jul' : "07",
        'Aug' : "08",
        'Sep' : "09", 
        'Oct' : "10",
        'Nov' : "11",
        'Dec' : "12"
	}[date]

# infoGraber helper function
# @item: json item to grab
# @return: {'url': 'https://page_link',
#           'BuySD': 9.99, (optional)
#           'BuyHD': 9.99, (optional)
#			'RentSD': 2.99, (optional)
#			'RentHD': 3.99, (optional)}
#          None if no result
def infoGraber(item, result):
	result["url"] = item["trackViewUrl"]
	if "trackHdPrice" in item and item["trackHdPrice"] != -1:
		result["BuyHD"] = item["trackHdPrice"]
	if "trackHdRentalPrice" in item and item["trackHdRentalPrice"] != -1:
		result["RentHD"] = item["trackHdRentalPrice"]
	if "trackPrice" in item and item["trackPrice"] != -1:
		result["BuySD"] = item["trackPrice"]
	if "trackRentalPrice" in item and item["trackRentalPrice"] != -1:
		result["RentSD"] = item["trackRentalPrice"]
	return result

# iTunesSeriesGraber
# @title: title of series
# @epTitle: title of episode
# @dateOMDb: release date from OMDb
# @return: {'url': 'https://page_link' (or 'None'),
#           'BuySD': 9.99, (optional)
#           'BuyHD': 9.99, (optional)
#			'RentSD': 2.99, (optional)
#			'RentHD': 3.99, (optional)
#           'itunes_series':'seriesid'}
#          None if no result
def iTunesSeriesGraber(seriesid, title, epTitle, dateOMDb):
	# print "enter Graber "+title+", "+epTitle+", "+dateOMDb
	titleEncode = encodeHelper(title)
	epTitleEncode = encodeHelper(epTitle)
	result = {}
	result['log'] = ''
	result["url"] = "None"
	result["itunes_series"] = seriesid
	url = "https://itunes.apple.com/search?media=tvShow&entity=tvEpisode&&term="+titleEncode+"%20"+epTitleEncode;
	
	try:
		response = urllib2.urlopen(url)
		jsonResult = json.load(response)
	except urllib2.HTTPError, e:
	    result['log'] += 'HTTPError = ' + str(e.code)
	    return result
	except urllib2.URLError, e:
	    result['log'] += 'URLError = ' + str(e.reason)
	    return result
	except httplib.HTTPException, e:
	    result['log'] += 'HTTPException'
	    return result
	except Exception:
		result['log'] += 'generic exception: ' + traceback.format_exc()
		return result

	if jsonResult["resultCount"] == 0:
		# try more possibilities
		if "Pilot" in epTitle:
			epTitle = "Pilot"
			# print epTitle
			result = iTunesSeriesGraber(title, epTitle, dateOMDb)
			if result['url'] != "None":
				return result
		if "Part" in epTitle:
			epTitle = epTitle.replace("Part", "Pt")
			# print epTitle
			result = iTunesSeriesGraber(title, epTitle, dateOMDb)
			if result['url'] != "None":
				return result
		return result
	else:
		dateOMDbList = dateOMDb.split()
		ddOMDb = dateOMDbList[0]
		mmOMDb = dateOMDbList[1]
		yyyyOMDb = dateOMDbList[2]
		resultList = jsonResult["results"]
		for item in resultList: # loop items in jsonResult for releaseDate
			ddItunes = item["releaseDate"].split("T")[0].split("-")[2]
			mmItunes = item["releaseDate"].split("T")[0].split("-")[1]
			yyyyItunes = item["releaseDate"].split("T")[0].split("-")[0]
			if ddOMDb == ddItunes and monthToNum(mmOMDb) == mmItunes and yyyyOMDb == yyyyItunes: # date match, return
				return infoGraber(item, result)
		for item in resultList: # loop items in jsonResult for epTitle
			if "trackName" in item and epTitle.upper() == item["trackName"].upper(): # epTitle match, return
				return infoGraber(item, result)
		return result # no date match


if __name__ == "__main__":
	seriesid = 'xxx'
	title = "family guy"
	epTitle = "Peter, Peter, Caviar Eater"
	dateOMDb = "23 Sep 1999"
	print iTunesSeriesGraber(seriesid, title, epTitle, dateOMDb)