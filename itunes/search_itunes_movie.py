# coding=utf-8
import urllib2
import urllib
import json
from urlparse import urlparse

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")
	
# infoGraber helper function
# @item: json item to grab
# @return: {'url': 'https://page_link',
#           'BuySD': 9.99, (optional)
#           'BuyHD': 9.99, (optional)
#			'RentSD': 2.99, (optional)
#			'RentHD': 3.99, (optional)}
#           'log':'xxx'
#          None if no result
def infoGraber(item, result):
	result["url"] = item["trackViewUrl"]
	if "trackHdPrice" in item and item["trackHdPrice"] != -1:
		result["BuyHD"] = item["trackHdPrice"]
	if "trackHdRentalPrice" in item and item["trackHdRentalPrice"] != -1:
		result["RentHD"] = item["trackHdRentalPrice"]
	if "trackPrice" in item and item["trackPrice"] != -1:
		result["BuySD"] = item["trackPrice"]
	if "trackRentalPrice" in item and item["trackRentalPrice"] != -1:
		result["RentSD"] = item["trackRentalPrice"]
	return result

# iTunesMovieGraber
# @title: title of series
# @year: release year
# @return: {'url': 'https://page_link' (or 'None'),
#           'BuySD': 9.99, (optional)
#           'BuyHD': 9.99, (optional)
#			'RentSD': 2.99, (optional)
#			'RentHD': 3.99, (optional)
#           'log': 'xxx'
#           'itunes_movie':'movieid'}
#          None if no result
def iTunesMovieGraber(movieid, title, year):
	title = encodeHelper(title)
	result = {}
	result['log'] = ''
	result["url"] = "None"
	result['itunes_movie'] = movieid
	url = "https://itunes.apple.com/search?media=movie&term="+title;
	try:
		response = urllib2.urlopen(url)
		jsonResult = json.load(response)
		if jsonResult["resultCount"] == 0:
			return result
		else:
			resultList = jsonResult["results"]
			for item in resultList: # loop items in jsonResult
				if item["releaseDate"].split("-")[0] == year: # year match, return
					return infoGraber(item, result)
			return result # no year match
	except urllib2.HTTPError, e:
	    result['log'] += 'HTTPError = ' + str(e.code)
	    return result
	except urllib2.URLError, e:
	    result['log'] += 'URLError = ' + str(e.reason)
	    return result
	except httplib.HTTPException, e:
	    result['log'] += 'HTTPException'
	    return result
	except Exception:
		result['log'] += 'generic exception: ' + traceback.format_exc()
		return result

if __name__ == "__main__":
	movieid = 'xxx'
	title = "the terminators"
	year = "2009"
	print iTunesMovieGraber(movieid, title, year)