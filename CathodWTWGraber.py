# Filename: CathodWTWGraber.py
# Author: Di Yang
# Email: twistedlogicuestc@gmail.com

from WhereToWatchModule import HuluMovieGraber, iTunesMovieGraber, AmazonMovieGraber, NetflixMovieGraber, HuluSeriesGraber, iTunesSeriesGraber, AmazonSeriesGraber, NetflixSeriesGraber

if __name__ == "__main__":
    # huluMovie = HuluMovieGraber("123", "The Terminators", "2009")
    # print huluMovie.getResultURL()
    # iTunesMovie = iTunesMovieGraber("123", "The Terminators", "2009")
    # print iTunesMovie.getResultURL()
    # amazonMovie = AmazonMovieGraber("123", "Star Wars: Episode VI - Return of the Jedi", "1983", "Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams")
    # print amazonMovie.getResultURL()
    # netflixMovie = NetflixMovieGraber("123", "The Terminator")
    # print netflixMovie.getResultURL()
    # huluSeries = HuluSeriesGraber("123", "family guy", "6", "1", "23 Sep 2007")
    # print huluSeries.getResultAll()
    # iTunesSeries = iTunesSeriesGraber("123", "lost", "LA X - Part 1", "02 Feb 2010")
    # print iTunesSeries.getResultAll()
    # amazonSeries = AmazonSeriesGraber("123", "Good Eats", "A Bowl of Onion")
    # print amazonSeries.getResultAll()
    # print "family guy 6 1 wait"
    # netflixSeries = NetflixSeriesGraber("sid1", "epid1", "Family guy", "Blue Harvest", "USA", "2007", "6", "1")
    # print netflixSeries.getResultAll()
    # print "family guy 6 2 no wait"
    # netflixSeries = NetflixSeriesGraber("sid1", "epid2", "Family guy", "Movin' Out (Brian's Song)", "USA", "2007", "6", "2")
    # print netflixSeries.getResultAll()
    # print "family guy 7 1 wait"
    # netflixSeries = NetflixSeriesGraber("sid1", "epid3", "Family guy", "Love Blactually", "USA", "2008", "7", "1")
    # print netflixSeries.getResultAll()
    # # print "lost 1 1 wait"
    # # netflixSeries = NetflixSeriesGraber("sid2", "epid4", "lost", "Pilot: Part 1", "USA", "2004", "1", "1")
    # # print netflixSeries.getResultAll()
    print "love it or list it 1 2 wait"
    netflixSeries = NetflixSeriesGraber("sid3", "epid5", "love it or list it", "Daddy Daycares", "USA", "2004", "1", "2")
    print netflixSeries.getCurrentResultAll()
    print netflixSeries.getResultAll()
    # print "love it or list it 7 1 no wait"
    # netflixSeries = NetflixSeriesGraber("sid3", "epid6", "love it or list it", "Boy Overload", "USA", "2004", "7", "1")
    # print netflixSeries.getResultAll()