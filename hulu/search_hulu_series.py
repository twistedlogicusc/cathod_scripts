# coding=utf-8
import urllib2
import urllib
import json
from urlparse import urlparse
import traceback

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	# print urlparse(string)
	# print resultString
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")

# helper fucntion, month to number
def monthToNum(date):
	return{
        'Jan' : "01",
        'Feb' : "02",
        'Mar' : "03",
        'Apr' : "04",
        'May' : "05",
        'Jun' : "06",
        'Jul' : "07",
        'Aug' : "08",
        'Sep' : "09", 
        'Oct' : "10",
        'Nov' : "11",
        'Dec' : "12"
	}[date]

# huluSeriesGraber
# @title: title of series
# @season: season
# @episode: episode
# @dateOMDb: release date from OMDb
# @return: {'url': 'https://page_link' (or "None"), "log":"xxx", 'hulu_series':'seriesid'}
def huluSeriesGraber(seriesid, title, season, episode, dateOMDb):
	result = {}
	result['log'] = ""
	result["url"] = "None"
	result['hulu_series'] = seriesid
	titleEncode = encodeHelper(title)
	dateOMDbList = dateOMDb.split()
	dd = dateOMDbList[0]
	mm = monthToNum(dateOMDbList[1])
	yyyy = dateOMDbList[2]
	url = "http://www.hulu.com/sapi/search/grid?query=\""+titleEncode+"\"+season%3A"+season+"+episode%3A"+episode+"+date%3A"+mm+"%2F"+dd+"%2F"+yyyy+"&type=episodes&items_per_page=64&position=0&_user_pgid=1&_content_pgid=29635&_device_id=1&region=us&locale=en&language=en&treatment=control";
	# print url
	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3','Accept-Encoding': 'none','Accept-Language': 'en-US,en;q=0.8','Connection': 'keep-alive'}
	# response = urllib2.urlopen(url)
	request = urllib2.Request(url,headers=hdr)
	jsonResult = {}
	try:
		response = urllib2.urlopen(request)
		jsonResult = json.load(response)
		if jsonResult["total_count"] == 0:
			return result
		else: # return the first one
			resultList = jsonResult["data"]
			result["url"] = "http://www.hulu.com/watch/"+str(resultList[0]["video"]["id"])
			return result
	except urllib2.HTTPError, e:
	    result['log'] += 'HTTPError = ' + str(e.code)
	    return result
	except urllib2.URLError, e:
	    result['log'] += 'URLError = ' + str(e.reason)
	    return result
	except httplib.HTTPException, e:
	    result['log'] += 'HTTPException'
	    return result
	except Exception:
		result['log'] += 'generic exception: ' + traceback.format_exc()
		return result

if __name__ == "__main__":
	seriesid = 'xxx'
	title = "family guys"
	season = "6"
	episode = "1"
	dateOMDb = "23 Sep 2007"
	print huluSeriesGraber(seriesid, title, season, episode, dateOMDb)