# coding=utf-8
import urllib2
import urllib
import json
from urlparse import urlparse

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	# print urlparse(string)
	# print resultString
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")


# huluMovieGraber
# @title: title of movie
# @year: yyyy
# @return: {'url': 'https://page_link' (or "None"), "log":"xxx", 'hulu_movie':'movieid'}
def huluMovieGraber(movieid, title, year):
	result = {}
	result['log'] = ''
	result["url"] = "None"
	result['hulu_movie'] = movieid
	titleEncode = encodeHelper(title)
	url = "http://www.hulu.com/sapi/search/grid?query="+titleEncode+"%20date%3A"+year+"&type=movies&items_per_page=64&position=0&_user_pgid=1&_content_pgid=29635&_device_id=1&region=us&locale=en&language=en&treatment=control";
	hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3','Accept-Encoding': 'none','Accept-Language': 'en-US,en;q=0.8','Connection': 'keep-alive'}
	try:
		response = urllib2.urlopen(urllib2.Request(url,headers=hdr))
		jsonResult = json.load(response)
		if jsonResult["total_count"] == 0:
			return result
		else: # return the first one
			resultList = jsonResult["data"]
			result["url"] = "http://www.hulu.com/watch/"+str(resultList[0]["video"]["id"])
			return result
	except urllib2.HTTPError, e:
	    result['log'] += 'HTTPError = ' + str(e.code)
	    return result
	except urllib2.URLError, e:
	    result['log'] += 'URLError = ' + str(e.reason)
	    return result
	except httplib.HTTPException, e:
	    result['log'] += 'HTTPException'
	    return result
	except Exception:
		result['log'] += 'generic exception: ' + traceback.format_exc()
		return result

if __name__ == "__main__":
	movieid = 'xxx'
	title = "The Terminatorssss"
	year = "2009"
	print huluMovieGraber(movieid, title, year)