# coding=utf-8
# Filename: WhereToWatchModule.py
# Author: Di Yang
# Email: twistedlogicuestc@gmail.com

###################### USAGE #########################
"""
    HuluMovieGraber(movieid, title, year)
    huluMovie = HuluMovieGraber("123", "The Terminators", "2009")
    huluMovie.getResultURL()

    iTunesMovieGraber(movieid, title, year)
    iTunesMovie = iTunesMovieGraber("123", "The Terminators", "2009")
    iTunesMovie.getResultURL()

    AmazonMovieGraber(movieid, title, year, actorOMDb)
    amazonMovie = AmazonMovieGraber("123", "Star Wars: Episode VI - Return of the Jedi", "1983", "Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams")
    amazonMovie.getResultURL()

    NetflixMovieGraber(movieid, title)
    netflixMovie = NetflixMovieGraber("123", "The Terminator")
    netflixMovie.getResultURL()

    HuluSeriesGraber(episodeid, title, season, episode, dateOMDb)
    huluSeries = HuluSeriesGraber("123", "family guy", "6", "1", "23 Sep 2007")
    huluSeries.getResultAll()

    iTunesSeriesGraber(episodeid, title, epTitle, dateOMDb)
    iTunesSeries = iTunesSeriesGraber("123", "lost", "LA X - Part 1", "02 Feb 2010")
    iTunesSeries.getResultAll()

    AmazonSeriesGraber(episodeid, title, epTitle)
    amazonSeries = AmazonSeriesGraber("123", "Good Eats", "A Bowl of Onion")
    amazonSeries.getResultAll()

    NetflixSeriesGraber(seriesid, episodeid, title, epTitle, country, year, season, episode, isCollection="False")
    netflixSeries = NetflixSeriesGraber("123", "Family guy", "USA", "2007", "6")
    netflixSeries.getResultAll()

"""
######################################################

import hashlib
import urllib2
import hmac
import base64
import urllib
import xml.etree.ElementTree as ET
import re
import httplib
from GlobalHelperFunction import encodeHelper, getTimestamp, monthToNum, iTunesInfoGraber, AmazonConfig
import json
import traceback
import subprocess

# BaseGraber class
class BaseGraber(object):
    def __init__(self):
        self._result = {}

    def grab(self):
        pass

    # some sources (netflix) need to transform id to URL 
    def idToURL(self):
        pass

    def getResultAll(self):
        self.idToURL()
        return self._result

    def getResultURL(self):
        self.idToURL()
        return self._result['url']

    def getResultLog(self):
        return self._result['log']

    def getResultId(self):
        pass

    def getChargeInfo(self):
        pass


# HuluMovieGraber
# getResultAll: {'url': 'https://page_link' (or 'None'), 'log':'xxx', 'hulu_movie':'movieid'}
class HuluMovieGraber(BaseGraber):
    def __init__(self, movieid, title, year):
        super(HuluMovieGraber, self).__init__()
        self.__movieid = movieid
        self.__title = title
        self.__year = year
        self.grab()
    
    def grab(self):
        self._result = self.huluMovieGrab(self.__movieid, self.__title, self.__year)

    def getResultId(self):
        return self._result['hulu_movie']

    # huluMovieGrab
    # @movieid: movieid
    # @title: title of movie
    # @year: yyyy
    # @return: {'url': 'https://page_link' (or 'None'), 'log':'xxx', 'hulu_movie':'movieid'}
    def huluMovieGrab(self, movieid, title, year):
        result = {}
        result['log'] = ''
        result["url"] = "None"
        result['hulu_movie'] = movieid
        titleEncode = encodeHelper(title)
        url = "http://www.hulu.com/sapi/search/grid?query="+titleEncode+"%20date%3A"+year+"&type=movies&items_per_page=64&position=0&_user_pgid=1&_content_pgid=29635&_device_id=1&region=us&locale=en&language=en&treatment=control";
        hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3','Accept-Encoding': 'none','Accept-Language': 'en-US,en;q=0.8','Connection': 'keep-alive'}
        try:
            response = urllib2.urlopen(urllib2.Request(url,headers=hdr))
            jsonResult = json.load(response)
            if jsonResult["total_count"] == 0:
                return result
            else: # return the first one
                resultList = jsonResult["data"]
                result["url"] = "http://www.hulu.com/watch/"+str(resultList[0]["video"]["id"])
                return result
        except urllib2.HTTPError, e:
            result['log'] += 'HTTPError = ' + str(e.code)
            return result
        except urllib2.URLError, e:
            result['log'] += 'URLError = ' + str(e.reason)
            return result
        except httplib.HTTPException, e:
            result['log'] += 'HTTPException'
            return result
        except Exception:
            result['log'] += 'generic exception: ' + traceback.format_exc()
            return result

# iTunesMovieGraber
class iTunesMovieGraber(BaseGraber):
    def __init__(self, movieid, title, year):
        super(iTunesMovieGraber, self).__init__()
        self.__movieid = movieid
        self.__title = title
        self.__year = year
        self.grab()
    
    def grab(self):
        self._result = self.iTunesMovieGrab(self.__movieid, self.__title, self.__year)

    def getResultId(self):
        return self._result['itunes_movie']

    def getChargeInfo(self):
        result = {}
        result['BuySD'] = self._result.get('BuySD', 'None')
        result['BuyHD'] = self._result.get('BuyHD', 'None')
        result['RentSD'] = self._result.get('RentSD', 'None')
        result['RentHD'] = self._result.get('RentHD', 'None')
        return result

    # iTunesMovieGrab
    # @title: title of series
    # @year: release year
    # @return: {'url': 'https://page_link' (or 'None'),
    #           'BuySD': 9.99, (optional)
    #           'BuyHD': 9.99, (optional)
    #           'RentSD': 2.99, (optional)
    #           'RentHD': 3.99, (optional)
    #           'log': 'xxx'
    #           'itunes_movie':'movieid'}
    #          None if no result
    def iTunesMovieGrab(self, movieid, title, year):
        encodeTitle = encodeHelper(title)
        result = {}
        result['log'] = ''
        result["url"] = "None"
        result['itunes_movie'] = movieid
        url = "https://itunes.apple.com/search?media=movie&term="+encodeTitle;
        try:
            response = urllib2.urlopen(url)
            jsonResult = json.load(response)
            if jsonResult["resultCount"] == 0:
                return result
            else:
                resultList = jsonResult["results"]
                for item in resultList: # loop items in jsonResult
                    if item["releaseDate"].split("-")[0] == year: # year match, return
                        return iTunesInfoGraber(item, result)
                return result # no year match
        except urllib2.HTTPError, e:
            result['log'] += 'HTTPError = ' + str(e.code)
            return result
        except urllib2.URLError, e:
            result['log'] += 'URLError = ' + str(e.reason)
            return result
        except httplib.HTTPException, e:
            result['log'] += 'HTTPException'
            return result
        except Exception:
            result['log'] += 'generic exception: ' + traceback.format_exc()
            return result

# AmazonMovieGraber
# getResultAll: {'url':'link' (or 'None'), 'log':'xxx', 'amazon_movie':',movieid'}
class AmazonMovieGraber(BaseGraber):
    def __init__(self, movieid, title, year, actorOMDb):
        super(AmazonMovieGraber, self).__init__()
        self.__movieid = movieid
        self.__title = title
        self.__year = year
        self.__actorOMDb = actorOMDb
        self.grab()
       
    
    def grab(self):
        self._result = self.AmazonMovieGrab(self.__movieid, self.__title, self.__year, self.__actorOMDb)
    
    def getResultId(self):
        return self._result['amazon_movie']

    # AmazonMovieGrab
    # @title: title of movie or series
    # @year: release year
    # @actorListString: the whole actor string
    # @return: -1: empty title
    #          {'url':'link' (or 'None'), 'log':'xxx', 'amazon_movie':',movieid'}
    def AmazonMovieGrab(self, movieid, title, year, actorListString):
        # print title
        if title == "":
            return -1 # empty title
        keywords = encodeHelper(title+" "+year)
        actorList = actorListString.split(", ")
        result = {}
        result['log'] = ''
        result["url"] = "None"
        result['amazon_movie'] = movieid
        # loop actor
        for i in range(0, len(actorList)):
            # print "actor looping"
            actor = encodeHelper(actorList[i])
            timestamp = getTimestamp()
            test_string = "GET\n" + "webservices.amazon.com\n" + "/onca/xml\n" + "AWSAccessKeyId="+AmazonConfig.awsAccessKeyId+ "&Actor="+actor+ "&AssociateTag="+AmazonConfig.associateTag+ "&BrowseNode="+AmazonConfig.browseNode+ "&Keywords="+keywords+ "&Operation="+AmazonConfig.operation_itemSearch + "&ResponseGroup="+AmazonConfig.responseGroup_itemSearch+ "&SearchIndex="+AmazonConfig.searchIndex+ "&Service="+AmazonConfig.service+ "&Timestamp="+timestamp+ "&Version=2013-08-01";

            # calculate signature2
            signature2 = base64.b64encode(hmac.new(AmazonConfig.awsSecretKey, test_string, digestmod=hashlib.sha256).digest())
            url = "http://webservices.amazon.com/onca/xml?" + "AWSAccessKeyId="+AmazonConfig.awsAccessKeyId+ "&AssociateTag="+AmazonConfig.associateTag+ "&BrowseNode="+AmazonConfig.browseNode+ "&Keywords="+keywords+ "&Actor="+actor+ "&Operation="+AmazonConfig.operation_itemSearch + "&ResponseGroup="+AmazonConfig.responseGroup_itemSearch+ "&SearchIndex="+AmazonConfig.searchIndex+ "&Service="+AmazonConfig.service+ "&Timestamp="+timestamp+ "&Version=2013-08-01"+ "&Signature="+urllib.quote(bytes(signature2).encode('utf-8')).replace("+", "%20").replace("*", "%2A").replace("%7E", "~");

            # open url and retrieve xml
            try:
                xml = urllib2.urlopen(url)
                resultXML = xml.read()
                xml.close()
                resultXML = re.sub(' xmlns="[^"]+"', '', resultXML, count=1)
                root = ET.fromstring(resultXML)
                if root.find("Items").find("TotalResults").text != "0": # exists result(s), must return the first result with ProductGroup = "Movie"
                    itemRoot = root.find("Items").findall("Item")
                    for item in itemRoot:
                        if item.find("ItemAttributes").find("ProductGroup").text == "Movie":
                            result['url'] = "http://www.amazon.com/dp/"+item.find("ASIN").text
                            return result
            except urllib2.HTTPError, e:
                result['log'] += 'HTTPError = ' + str(e.code)
                return result
            except urllib2.URLError, e:
                result['log'] += 'URLError = ' + str(e.reason)
                return result
            except httplib.HTTPException, e:
                result['log'] += 'HTTPException'
                return result
            except Exception:
                result['log'] += 'generic exception: ' + traceback.format_exc()
                return result
        result = self.specialCaseCheck(movieid, title, year, actorListString, result)
        return result

    # Movie Special Case -- "Star Wars: Episode VI - Return of the Jedi"
    # Amazon -- "Star Wars: Return of the Jedi"
    # One last try! "Return of the Jedi"
    def specialCaseCheck(self, movieid, title, year, actorListString, result):
        newTitleSplitByComma = title.split(":")
        newTitleSplitByDash = title.split("-")
        resultComma = ""
        resultDash = ""
        if len(newTitleSplitByComma) != 1:
            # print "enter comma"
            newTitle = newTitleSplitByComma[len(newTitleSplitByComma)-1].strip()
            # print newTitle
            resultComma =  self.AmazonMovieGrab(movieid, newTitle, year, actorListString)
            if resultComma != -1 and resultComma['url'] != "None": # if return a link
                return resultComma
        if len(newTitleSplitByDash) != 1:
            # print "enter dash"
            newTitle = newTitleSplitByDash[len(newTitleSplitByDash)-1].strip()
            # print newTitle
            resultDash =  self.AmazonMovieGrab(movieid, newTitle, year, actorListString)
            if resultDash != -1 and resultDash['url'] != 'None': # if return a link
                return resultDash
        return result

# NetflixMovieGraber
# getResultAll: {'url': 'https://page_link' (or 'None'), 
#                'log':'STEP-1 -- ....', 
#                'netflix_movie':'movieid'}
class NetflixMovieGraber(BaseGraber):
    def __init__(self, movieid, title):
        super(NetflixMovieGraber, self).__init__()
        self.__movieid = movieid
        self.__title = title
        self.grab()

    def grab(self):
        self._result = self.netflixMovieGrab(self.__movieid, self.__title)


    def getResultId(self):
        return self._result['netflix_movie']

    # netflixMovieGrab
    # @title: title of movie
    # @return: {'url': 'https://page_link' (or "None"), 
    #           "log":"STEP-1 -- ....", 
    #           "netflix_movie":'movieid'}
    def netflixMovieGrab(self, movieid, title):
        title = encodeHelper(title)
        proc = subprocess.Popen('node netflix_movie.js'+" "+title, stdout=subprocess.PIPE, shell=True)
        response = eval(re.sub(r"\n", " ", proc.stdout.read()))
        response["netflix_movie"] = movieid
        return response

    def idToURL(self):
        if self._result['url'] != "None":
            self._result['url'] = "http://www.netflix.com/watch/"+self._result['url']



# HuluSeriesGraber
# getResultAll: {'url': 'https://page_link' (or "None"), "log":"xxx", 'hulu_episode':'episodeid'}
class HuluSeriesGraber(BaseGraber):
    def __init__(self, episodeid, title, season, episode, dateOMDb):
        super(HuluSeriesGraber, self).__init__()
        self.__episodeid = episodeid
        self.__title = title
        self.__season = season
        self.__episode = episode
        self.__dateOMDb = dateOMDb
        self.grab()

    def grab(self):
        self._result = self.huluSeriesGrab(self.__episodeid, self.__title, self.__season, self.__episode, self.__dateOMDb)

    def getResultId(self):
        return self._result['hulu_episode']

    # huluSeriesGrab
    # @episodeid
    # @title: title of series
    # @season: season
    # @episode: episode
    # @dateOMDb: release date from OMDb
    # @return: {'url': 'https://page_link' (or "None"), "log":"xxx", 'hulu_episode':'episodeid'}
    def huluSeriesGrab(self, episodeid, title, season, episode, dateOMDb):
        result = {}
        result['log'] = ""
        result["url"] = "None"
        result['hulu_episode'] = episodeid
        titleEncode = encodeHelper(title)
        dateOMDbList = dateOMDb.split()
        dd = dateOMDbList[0]
        mm = monthToNum(dateOMDbList[1])
        yyyy = dateOMDbList[2]
        url = "http://www.hulu.com/sapi/search/grid?query=\""+titleEncode+"\"+season%3A"+season+"+episode%3A"+episode+"+date%3A"+mm+"%2F"+dd+"%2F"+yyyy+"&type=episodes&items_per_page=64&position=0&_user_pgid=1&_content_pgid=29635&_device_id=1&region=us&locale=en&language=en&treatment=control";
        # print url
        hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11','Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8','Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3','Accept-Encoding': 'none','Accept-Language': 'en-US,en;q=0.8','Connection': 'keep-alive'}
        # response = urllib2.urlopen(url)
        request = urllib2.Request(url,headers=hdr)
        jsonResult = {}
        try:
            response = urllib2.urlopen(request)
            jsonResult = json.load(response)
            if jsonResult["total_count"] == 0:
                return result
            else: # return the first one
                resultList = jsonResult["data"]
                result["url"] = "http://www.hulu.com/watch/"+str(resultList[0]["video"]["id"])
                return result
        except urllib2.HTTPError, e:
            result['log'] += 'HTTPError = ' + str(e.code)
            return result
        except urllib2.URLError, e:
            result['log'] += 'URLError = ' + str(e.reason)
            return result
        except httplib.HTTPException, e:
            result['log'] += 'HTTPException'
            return result
        except Exception:
            result['log'] += 'generic exception: ' + traceback.format_exc()
            return result

# iTunesSeriesGraber
# getResultAll: {'url': 'https://page_link' (or 'None'),
#                'BuySD': 9.99, (optional)
#                'BuyHD': 9.99, (optional)
#                'RentSD': 2.99, (optional)
#                'RentHD': 3.99, (optional)
#                'itunes_episode':'episodeid'
#               }
class iTunesSeriesGraber(BaseGraber):
    def __init__(self, episodeid, title, epTitle, dateOMDb):
        super(iTunesSeriesGraber, self).__init__()
        self.__episodeid = episodeid
        self.__title = title
        self.__epTitle = epTitle
        self.__dateOMDb = dateOMDb
        self.grab() 

    def grab(self):
        self._result = self.iTunesSeriesGrab(self.__episodeid, self.__title, self.__epTitle, self.__dateOMDb)

    def getResultId(self):
        return self._result['itunes_episode']

    # iTunesSeriesGrab
    # @episodeid
    # @title: title of series
    # @epTitle: title of episode
    # @dateOMDb: release date from OMDb
    # @return: {'url': 'https://page_link' (or 'None'),
    #           'BuySD': 9.99, (optional)
    #           'BuyHD': 9.99, (optional)
    #           'RentSD': 2.99, (optional)
    #           'RentHD': 3.99, (optional)
    #           'itunes_episode':'episodeid'
    #          }
    def iTunesSeriesGrab(self, episodeid, title, epTitle, dateOMDb):
        # print "enter Graber "+title+", "+epTitle+", "+dateOMDb
        titleEncode = encodeHelper(title)
        epTitleEncode = encodeHelper(epTitle)
        result = {}
        result['log'] = ''
        result["url"] = "None"
        result["itunes_episode"] = episodeid
        url = "https://itunes.apple.com/search?media=tvShow&entity=tvEpisode&&term="+titleEncode+"%20"+epTitleEncode;
        
        try:
            response = urllib2.urlopen(url)
            jsonResult = json.load(response)
        except urllib2.HTTPError, e:
            result['log'] += 'HTTPError = ' + str(e.code)
            return result
        except urllib2.URLError, e:
            result['log'] += 'URLError = ' + str(e.reason)
            return result
        except httplib.HTTPException, e:
            result['log'] += 'HTTPException'
            return result
        except Exception:
            result['log'] += 'generic exception: ' + traceback.format_exc()
            return result

        if jsonResult["resultCount"] == 0:
            # try more possibilities
            if "Pilot" in epTitle:
                epTitle = "Pilot"
                # print epTitle
                result = self.iTunesSeriesGrab(episodeid, title, epTitle, dateOMDb)
                if result['url'] != "None":
                    return result
            if "Part" in epTitle:
                epTitle = epTitle.replace("Part", "Pt")
                # print epTitle
                result = self.iTunesSeriesGrab(episodeid, title, epTitle, dateOMDb)
                if result['url'] != "None":
                    return result
            return result
        else:
            dateOMDbList = dateOMDb.split()
            ddOMDb = dateOMDbList[0]
            mmOMDb = dateOMDbList[1]
            yyyyOMDb = dateOMDbList[2]
            resultList = jsonResult["results"]
            for item in resultList: # loop items in jsonResult for releaseDate
                ddItunes = item["releaseDate"].split("T")[0].split("-")[2]
                mmItunes = item["releaseDate"].split("T")[0].split("-")[1]
                yyyyItunes = item["releaseDate"].split("T")[0].split("-")[0]
                if ddOMDb == ddItunes and monthToNum(mmOMDb) == mmItunes and yyyyOMDb == yyyyItunes: # date match, return
                    return iTunesInfoGraber(item, result)
            for item in resultList: # loop items in jsonResult for epTitle
                if "trackName" in item and epTitle.upper() == item["trackName"].upper(): # epTitle match, return
                    return iTunesInfoGraber(item, result)
            return result # no date match


# AmazonSeriesGraber
# getResultAll: {'url':'link' (or 'None'), 'log':'xxx', 'amazon_episode':'episodeid'}
class AmazonSeriesGraber(BaseGraber):
    def __init__(self, episodeid, title, epTitle):
        super(AmazonSeriesGraber, self).__init__()
        self.__episodeid = episodeid
        self.__title = title
        self.__epTitle = epTitle
        self.grab()

    def grab(self):
        self._result = self.AmazonSeriesGrab(self.__episodeid, self.__title, self.__epTitle)

    def getResultId(self):
        return self._result['amazon_episode']

    # AmazonSeriesGrab
    # @episodeid
    # @title: title series
    # @epTitle: episode title
    # @return: -1: empty title
    #          {'url':'link' (or 'None'), 'log':'xxx', 'amazon_episode':'episodeid'}
    def AmazonSeriesGrab(self, episodeid, title, epTitle):
        # print title
        if title == "":
            return -1 # empty title
        keywords =  encodeHelper(title+" "+epTitle)
        result = {}
        result['log'] = ''
        result["url"] = "None"
        result['amazon_episode'] = episodeid
        timestamp = getTimestamp()
        test_string = "GET\n" + "webservices.amazon.com\n" + "/onca/xml\n" + "AWSAccessKeyId="+AmazonConfig.awsAccessKeyId+ "&AssociateTag="+AmazonConfig.associateTag+ "&Keywords="+keywords+ "&Operation="+AmazonConfig.operation_itemSearch + "&ResponseGroup="+AmazonConfig.responseGroup_itemSearch+ "&SearchIndex="+AmazonConfig.searchIndex+ "&Service="+AmazonConfig.service+ "&Timestamp="+timestamp+ "&Version=2013-08-01";

        # calculate signature2
        signature2 = base64.b64encode(hmac.new(AmazonConfig.awsSecretKey, test_string, digestmod=hashlib.sha256).digest())
        url = "http://webservices.amazon.com/onca/xml?" + "AWSAccessKeyId="+AmazonConfig.awsAccessKeyId+ "&AssociateTag="+AmazonConfig.associateTag+ "&Keywords="+keywords+ "&Operation="+AmazonConfig.operation_itemSearch + "&ResponseGroup="+AmazonConfig.responseGroup_itemSearch+ "&SearchIndex="+AmazonConfig.searchIndex+ "&Service="+AmazonConfig.service+ "&Timestamp="+timestamp+ "&Version=2013-08-01"+ "&Signature="+urllib.quote(bytes(signature2).encode('utf-8')).replace("*", "%2A").replace("%7E", "~");

        # open url and retrieve xml
        try:
            xml = urllib2.urlopen(url)
            resultXML = xml.read()
            xml.close()
            resultXML = re.sub(' xmlns="[^"]+"', '', resultXML, count=1)
            root = ET.fromstring(resultXML)
            # print result
            if root.find("Items").find("TotalResults").text != "0": # exists result(s), must return the first result with productGroup = "TV Series Episode Video on Demand"
                itemRoot = root.find("Items").findall("Item")
                for item in itemRoot:
                    if item.find("ItemAttributes").find("ProductGroup").text == "TV Series Episode Video on Demand":
                        result['url'] = "http://www.amazon.com/dp/"+item.find("ASIN").text
                        return result
            return result
        except urllib2.HTTPError, e:
            result['log'] += 'HTTPError = ' + str(e.code)
            return result
        except urllib2.URLError, e:
            result['log'] += 'URLError = ' + str(e.reason)
            return result
        except httplib.HTTPException, e:
            result['log'] += 'HTTPException'
            return result
        except Exception:
            result['log'] += 'generic exception: ' + traceback.format_exc()
            return result   

# NetflixSeriesGraber
# store episodes for a season (not a collection) or the whole collection
# non-collection
#   getResultAll: {'url':'link' (or 'None'), 'log':'xxx', 'netflix_episode':'episodeid'}
#   getCurrentResultAll: { 'episodes': {'epNum':'12', '1':{'url':'xxx', 'xxx(epTitle)':'xxx'}, '2':...},
#                          'log':'...', 
#                          'netflix_series':'seriesid'
#                        } 
# collection:
#   getResultAll: {'url':'link' (or 'None'), 'log':'xxx', 'netflix_episode':'episodeid'}
#   getCurrentResultAll: { 'episodes':{'epNum':'35', 'xxx(epTitle)':{'url':'xxx'}, 'xxx':{'url':'xxx'}, ...}, 
#                          'log':'...',
#                          'netflix_series':'seriesid',
#                          'collectionSeasonList':[1,2,3]
#                        }
class NetflixSeriesGraber(BaseGraber):
    # class variable
    currentSeriesId = ''
    currentSeason = ''
    currentIsCollection = False
    currentResult = {}

    def __init__(self, seriesid, episodeid, title, epTitle, country, year, season, episode, isCollection="False"):
        super(NetflixSeriesGraber, self).__init__()
        self.__seriesid = seriesid
        self.__episodeid = episodeid
        self.__title = title
        self.__epTitle = epTitle
        self.__country = country
        self.__year = year
        self.__season = season
        self.__episode = episode
        self.__isCollection = isCollection
        # determine if grab is needed
        if self.startNewSeason(seriesid, season) == True:
            print "grab!!!!"
            self.grab()
        # set class variable for all instances
        NetflixSeriesGraber.currentSeriesId = seriesid
        NetflixSeriesGraber.currentSeason = season
        self.updateSelfResult() 

    def grab(self):
        NetflixSeriesGraber.currentResult = self.netflixSeriesGrab(self.__seriesid, self.__title, self.__country, self.__year, self.__season, self.__isCollection)
        if "collectionSeasonList" in NetflixSeriesGraber.currentResult:
            NetflixSeriesGraber.currentIsCollection = True
            self.crawCollection()
    
    def getResultId(self):
        return self._result['netflix_episode']

    def idToURL(self):
        if self._result['url'] != "None":
            self._result['url'] = "http://www.netflix.com/watch/"+self._result['url']

    # update self._result      
    def updateSelfResult(self):
        self._result['log'] = NetflixSeriesGraber.currentResult['log']
        self._result['netflix_episode'] = self.__episodeid
        if self.getCurrentIsCollection() == True:
            self._result['url'] = NetflixSeriesGraber.currentResult['episodes'].get(self.__epTitle, {}).get("url", "None")
        elif self.getCurrentIsCollection() == False:
            self._result['url'] = NetflixSeriesGraber.currentResult['episodes'].get(self.__episode, {}).get("url", "None")

    # netflixSeriesGraber
    # @seriesid: seriesid
    # @isCollection: default "False"; only "True" when used for loop collections
    # @title: title of series
    # @year: release year
    # @country: release country
    # @season: season to search
    # @return: {"log":"...", "episodes": {"1":{"url":"https://page_url", "epTitle":"xxx"}, ..., "epNum":"12"}(, "collectionSeasonList":[2, 3] for loop, or [-1] for no need to choose), 'netflix_series':'seriesid'} 
    def netflixSeriesGrab(self, seriesid, title, country, year, season, isCollection):
        title = encodeHelper(title)
        proc = subprocess.Popen('node netflix_series.js'+" "+isCollection+" "+title+" "+country+" "+year+" "+season, stdout=subprocess.PIPE, shell=True)
        # print re.sub(r"\n", "", proc.stdout.read())
        response = eval(re.sub(r"\n", "", proc.stdout.read()))
        response["netflix_series"] = seriesid
        return response

    # crawCollection
    # loop all collection seasons and store everything in seasonResult
    # seasonResult: {'episodes':{'epNum':'35', 'epTitle':{'url':'xxx'}, 'epTitle':{'url':'xxx'}, ...}, 
    #                'log':'...',
    #                'netflix_series':'xxx',
    #                'collectionSeasonList':[1,2,3]
    #               }
    def crawCollection(self):
        for s in NetflixSeriesGraber.currentResult['collectionSeasonList']:
            tempResult = self.netflixSeriesGrab(self.__seriesid, self.__title, self.__country, self.__year, s, "True")
            NetflixSeriesGraber.currentResult['log'] += tempResult['log']
            NetflixSeriesGraber.currentResult['episodes']['epNum'] = str(int(NetflixSeriesGraber.currentResult['episodes']['epNum'])+int(tempResult['episodes']['epNum']))
            for k,v in tempResult['episodes'].items():
                if k != "epNum":
                    NetflixSeriesGraber.currentResult['episodes'][v['epTitle']] = {}
                    NetflixSeriesGraber.currentResult['episodes'][v['epTitle']]['url'] =  v['url']

    # get the current class result
    def getCurrentResultAll(self):
        return NetflixSeriesGraber.currentResult

    def getCurrentSeriesID(self):
        return NetflixSeriesGraber.currentSeriesId

    def getCurrentSeason(self):
        return NetflixSeriesGraber.currentSeason

    # True/False
    def getCurrentIsCollection(self):
        return NetflixSeriesGraber.currentIsCollection

    # Use series title and season to determine whether to start crawling a new season
    # for collection, only use title because the whole collection will be considered as one season
    # title: series title
    # season: season
    # @return True/False
    def startNewSeason(self, seriesid, season):
        if self.getCurrentIsCollection() == True:
            if seriesid == self.getCurrentSeriesID():
                return False
            else:
                return True
        elif self.getCurrentIsCollection() == False:  
            if seriesid == self.getCurrentSeriesID() and season == self.getCurrentSeason():
                return False
            else:
                return True
