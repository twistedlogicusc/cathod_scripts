# coding=utf-8
import hashlib
import urllib2
import hmac
import base64
import datetime
import urllib
import xml.etree.ElementTree as ET
import re
from urlparse import urlparse

# helper function, encoding for amazon signing
def encodeHelper(string):
	resultString = ""
	if urlparse(string).scheme != "":
		resultString += urlparse(string).scheme+":"
	if urlparse(string).netloc != "":
		resultString += "//"+urlparse(string).netloc
	if urlparse(string).path != "":
		resultString += urlparse(string).path
	if urlparse(string).params != "":
		resultString += ";"+urlparse(string).params
	if urlparse(string).query != "":
		resultString += "?"+urlparse(string).query
	if urlparse(string).fragment != "":
		resultString += "#"+urlparse(string).fragment
	return urllib.quote(bytes(resultString).decode('utf-8').encode('utf-8')).replace("%7E", "~")

# AmazonSeriesGraber
# @title: title series
# @epTitle: episode title
# @return: -1: empty title
#          {'url':'link' (or 'None'), 'log':'xxx', 'amazon_series':'seriesid'}
def AmazonSeriesGraber(seriesid, title, epTitle):
	# print title
	if title == "":
		return -1 # empty title
	keywords =	encodeHelper(title+" "+epTitle)
	actorList = ["onlyOneElement"] # no need actor, so only loop actor once
	browseNode = "2858778011" # instant video, need to check now and then!!!!!!!
	responseGroup_itemSearch = encodeHelper("ItemAttributes")
	service = "AWSECommerceService"
	searchIndex = "VHS"
	associateTag = "cathod07-20"
	awsAccessKeyId = urllib.quote(bytes("AKIAIPOUFFWOLIKM7MFA").encode('utf-8'))
	awsSecretKey = urllib.quote(bytes("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").encode('utf-8'))
	operation_itemSearch = "ItemSearch"
	result = {}
	result['log'] = ''
	result["url"] = "None"
	result['amazon_series'] = seriesid
	timestamp_raw = datetime.datetime.utcnow().isoformat().split(".")
	timestamp = urllib.quote(bytes(timestamp_raw[0]+"Z").encode('utf-8'))
	test_string = "GET\n" + "webservices.amazon.com\n" + "/onca/xml\n" + "AWSAccessKeyId="+awsAccessKeyId+ "&AssociateTag="+associateTag+ "&Keywords="+keywords+ "&Operation="+operation_itemSearch + "&ResponseGroup="+responseGroup_itemSearch+ "&SearchIndex="+searchIndex+ "&Service="+service+ "&Timestamp="+timestamp+ "&Version=2013-08-01";

	# calculate signature2
	signature2 = base64.b64encode(hmac.new(awsSecretKey, test_string, digestmod=hashlib.sha256).digest())
	url = "http://webservices.amazon.com/onca/xml?" + "AWSAccessKeyId="+awsAccessKeyId+ "&AssociateTag="+associateTag+ "&Keywords="+keywords+ "&Operation="+operation_itemSearch + "&ResponseGroup="+responseGroup_itemSearch+ "&SearchIndex="+searchIndex+ "&Service="+service+ "&Timestamp="+timestamp+ "&Version=2013-08-01"+ "&Signature="+urllib.quote(bytes(signature2).encode('utf-8')).replace("*", "%2A").replace("%7E", "~");

	# open url and retrieve xml
	try:
		xml = urllib2.urlopen(url)
		resultXML = xml.read()
		xml.close()
		resultXML = re.sub(' xmlns="[^"]+"', '', resultXML, count=1)
		root = ET.fromstring(resultXML)
		# print result
		if root.find("Items").find("TotalResults").text != "0": # exists result(s), must return the first result with productGroup = "TV Series Episode Video on Demand"
			itemRoot = root.find("Items").findall("Item")
			for item in itemRoot:
				if item.find("ItemAttributes").find("ProductGroup").text == "TV Series Episode Video on Demand":
					result['url'] = "http://www.amazon.com/dp/"+item.find("ASIN").text
					return result
		return result
	except urllib2.HTTPError, e:
	    result['log'] += 'HTTPError = ' + str(e.code)
	    return result
	except urllib2.URLError, e:
	    result['log'] += 'URLError = ' + str(e.reason)
	    return result
	except httplib.HTTPException, e:
	    result['log'] += 'HTTPException'
	    return result
	except Exception:
		result['log'] += 'generic exception: ' + traceback.format_exc()
		return result
		
if __name__ == "__main__":
	seriesid = 'xxx'
	title = "Good Eats"
	epTitle = "A Bowl of Onion"
	print AmazonSeriesGraber(seriesid, title, epTitle)