// Filename: netflix_movie.js
// Author: Di Yang
// Email: twistedlogicuestc@gmail.com

/*
  Usage: Usage (command-line): node netflix_movie.js Title
  -c: cookie (optional)
*/
var url_netflix_search = "";
var resultObj = {};
var title = "";
var found = false;

var Nightmare = require('nightmare');
//check if a search parameter is correct
if (process.argv.length < 3) {
	console.log('Usage: node netflix_series.js Title');
	return;
}else {
    var length = process.argv.length;
    for (var i=2; i<length-1; i++){
        title += process.argv[i] + " ";
    }
	
    title += process.argv[length-1];
    url_netflix_search = "http://www.netflix.com/api/desktop/search/instantsearch?esn=www&term="+title;
    title = decodeURIComponent(title)
    console.log('{"log":')
    console.log('"STEP-1 Login and search for title...');
}
new Nightmare({cookiesFile: "./cookie.txt"})
    .on('timeout', function(msg) {
        return console.log(msg);
    })
    .viewport(1366, 768)
    .on('error', function(msg, trace) {
        return console.log("NETWORK_ERROR");
    })
    .useragent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0")
    // STEP-1 Login & Search
    .goto(url_netflix_search)
    // .screenshot('./1_afterSearch.png')
    .evaluate(function (title, country) {
            if (!JSON.parse(document.getElementsByTagName("pre")[0].textContent).hasOwnProperty('galleryVideos')) {
                return -1;
            }
            var showList = JSON.parse(document.getElementsByTagName("pre")[0].textContent).galleryVideos.items;
            if (showList.length == 1) { // only one match, return
                return showList[0].id;
            } else { // multiple results
                // star wars: the clone wars
                minID = 9007199254740992
                for (var i = 0; i < showList.length; i++) { // choose the smallest id (assume it's movie)
                    if (showList[i].title.toUpperCase() === title.toUpperCase() && minID > showList[i].id) {
                        minID = showList[i].id;
                    }
                }
                return minID == 9007199254740992 ? -2 : minID; // exist perfect match!
            }
        }, function (result) {
            if (result == -1) {
                console.log("STEP-2 NO RESULT!");
            } else if (result == -2) {
                console.log("STEP-2 multiple results but no match!");
            } else if (result != null) {
                found = true;
                resultObj = result;
                console.log("STEP-2 Getting result...")
            } else {
                console.log("OTHER ERRORS");
            }
        }, title
    )
    .run(function (err, nightmare) {
    	if (err) return console.log(err);
        // STEP-4 Result
        if (found) {
            console.log('","url":');
            console.log('"'+resultObj+'"}');
        }else { // no result key in json
            console.log('","url":"None"}')
        }
    });